module gitlab.com/stp-team/congo

go 1.19

require github.com/go-ini/ini v1.67.0

require github.com/stretchr/testify v1.8.0 // indirect
